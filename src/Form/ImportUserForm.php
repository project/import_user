<?php

namespace Drupal\import_user\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\import_user\Modal\ImportUser;
use Drupal\import_user\Helper\ImportUserHelper;
use JamesGordo\CSV\Parser;

/**
 *
 */
class ImportUserForm extends FormBase {


  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'import_user_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $importUserHelper = new ImportUserHelper();
	
    if ($form_state->has('page_num') && $form_state->get('page_num') == 2) {
      return self::preViewPageTwo($form, $form_state);
    }

    $form_state->set('page_num', 1);

    $form['description'] = [
      '#type' => 'item',
      '#title' => $this->t('User import from CSV source'),
    ];
	
	$form['user_upload'] = [
	  '#title' => 'File',
	  '#type' => 'managed_file',
	  '#description' => t('Select the CSV file to be imported.'),
	  '#upload_location' => 'public://import_user/',
	  '#multiple' => FALSE,
	  '#upload_validators' => [
		'file_validate_extensions' => ['csv'],
		'file_validate_size' => [25600],
	  ],
	  //'#required' => TRUE,
	];

    $form['field_delimiter'] = [
      '#type' => 'select',
	  '#options' => $importUserHelper->field_delimiter_options(),
      '#title' => $this->t('Field delimiter'),
      '#description' => $this->t('Select field delimiter. Comma is typical for CSV export files.'),
      '#default_value' => $form_state->getValue('field_delimiter', ''),      
    ];

    $form['value_delimiter'] = [
      '#type' => 'select',
	  '#options' => $importUserHelper->value_delimiter(),
      '#title' => $this->t('Value delimiter'),
      '#default_value' => $form_state->getValue('value_delimiter', ''),
      '#description' => $this->t('Select value delimiter for fields receiving multiple values.'),
    ];

    $form['preview_count'] = [
      '#type' => 'select',
	  '#options' => array('10' => '10', '100' => '100', '1000' => '1000'),
      '#title' => $this->t('Preview Count'),
      '#default_value' => $form_state->getValue('preview_count', ''),
      '#description' => $this->t('Number of users to preview before importing.'),
    ];

    $form['notify_user'] = [
      '#type' => 'checkbox',	  
      '#title' => $this->t('Notify new users of account'),
      '#default_value' => $form_state->getValue('notify_user', ''),
      '#description' => $this->t('If checked, each newly created user will receive the Welcome, new user created by administrator email'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['next'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Next'),      
      '#submit' => ['::multistepFormNextSubmit'],      
      '#validate' => ['::multistepFormNextValidate'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $page_values = $form_state->get('page_values');

    $this->messenger()->addMessage($this->t('The form has been submitted. file=@file, name="@first @last", year of birth=@year_of_birth', [
      '@first' => $page_values['field_delimiter'],
      '@last' => $page_values['value_delimiter'],
      '@year_of_birth' => $page_values['preview_count'],
	  '@file' => $page_values['user_upload'],
    ]));

    $this->messenger()->addMessage($this->t('And the favorite color is @color', ['@color' => $form_state->getValue('color')]));
	
	$importUserHelper = new ImportUserHelper();
	$users = new Parser('http://drupal8.com/sites/default/files/import_user/user.csv');

	// loop through each user and echo the details
	foreach($users->all() as $user) {		
		echo "User Details: {$user->mail} | {$user->name} | {$user->pass} | {$user->created} | {$user->access} {$user->login} | {$user->status}";
	}
	die;
  }

  /**
   * Provides custom validation handler for page 1.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function multistepFormNextValidate(array &$form, FormStateInterface $form_state) {
    $birth_year = $form_state->getValue('birth_year');

    if ($birth_year != '' && ($birth_year < 1900 || $birth_year > 2000)) {
      // Set an error for the form element with a key of "birth_year".
      $form_state->setErrorByName('birth_year', $this->t('Enter a year between 1900 and 2000.'));
    }
  }

  /**
   * Provides custom submission handler for page 1.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function multistepFormNextSubmit(array &$form, FormStateInterface $form_state) {
	  
	  //print_r(reset($form_state->getValue('user_upload')));die;
    $form_state
      ->set('page_values', [
        // Keep only first step values to minimize stored data.
        'field_delimiter' => $form_state->getValue('field_delimiter'),
        'value_delimiter' => $form_state->getValue('value_delimiter'),
        'preview_count' => $form_state->getValue('preview_count'),
        'user_upload' => reset($form_state->getValue('user_upload')),
      ])
      ->set('page_num', 2)
      ->setRebuild(TRUE);
  }

  /**
   * Builds the second step form (page 2).
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function preViewPageTwo(array &$form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'item',
      '#title' => $this->t('A basic multistep form (page 2)'),
    ];

    $form['color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Favorite color'),
      '#required' => TRUE,
      '#default_value' => $form_state->getValue('color', ''),
    ];
    $form['back'] = [
      '#type' => 'submit',
      '#value' => $this->t('Back'),
      // Custom submission handler for 'Back' button.
      '#submit' => ['::preViewPageTwoBack'],
      // We won't bother validating the required 'color' field, since they
      // have to come back to this page to submit anyway.
      '#limit_validation_errors' => [],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Provides custom submission handler for 'Back' button (page 2).
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function preViewPageTwoBack(array &$form, FormStateInterface $form_state) {
    $form_state
      // Restore values for the first step.
      ->setValues($form_state->get('page_values'))
      ->set('page_num', 1)
      // Since we have logic in our buildForm() method, we have to tell the form
      // builder to rebuild the form. Otherwise, even though we set 'page_num'
      // to 1, the AJAX-rendered form will still show page 2.
      ->setRebuild(TRUE);
  }
}
