<?php

namespace Drupal\import_user\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;


/**
 *
 */
class ImportUserForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'import_user_form';
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Cause return to beginning if we just completed an import.
    if (!$form_state->getStorage() && $form_state->getStorage() >= 3) {
      // unset($form_state->getStorage());
    }

    $step = !$form_state->getStorage() ? 1 : $form_state->getStorage();
    // $form_state->setStorage($step);
    switch ($step) {
      case 1:
        $form['instructions'] = [
          '#type' => 'details',
          '#title' => t('User import help'),
          '#open' => TRUE,
        ];

        $template = [
          '#theme' => 'import_user_form_help',
          '#data' => $this->uif_supported_fields(),
        ];

        $form['instructions']['help'] = [
          '#markup' => \Drupal::service('renderer')->render($template),
        ];

        $file_size_msg = t(
          'Your PHP settings limit the maximum file size per upload to %size. Depending on your server environment, these settings may be changed in the system-wide php.ini file, a php.ini file in your Drupal root directory, in your Drupal site\'s settings.php file, or in the .htaccess file in your Drupal root directory.', [
            '%size' => format_size(file_upload_max_size()),
          ]
        );

//        $form['user_upload'] = [
//          '#type' => 'file',
//          '#title' => t('Import file'),
//          '#size' => 40,
//          '#description' => t('Select the CSV file to be imported.') . '<br />' . $file_size_msg,
//        ];
        $form['user_upload'] = [
          '#title' => 'file',
          '#type' => 'managed_file',
          '#description' => t('Uploaded image can render using domain_logo variable in template'),
          '#upload_location' => 'public://import_user/',
          '#multiple' => FALSE,
          '#upload_validators' => [
            'file_validate_extensions' => ['csv'],
            'file_validate_size' => [25600],
          ],
        ];
        $form['field_delimiter'] = [
          '#type' => 'select',
          '#title' => t('Field delimiter'),
          '#default_value' => \Drupal::config('uif.settings')
            ->get('uif_field_delimiter'),
          '#options' => $this->uif_field_delimiters(),
          '#description' => t('Select field delimiter. Comma is typical for CSV export files.'),
        ];
        $form['value_delimiter'] = [
          '#type' => 'select',
          '#title' => t('Value delimiter'),
          '#default_value' => \Drupal::config('uif.settings')
            ->get('uif_value_delimiter'),
          '#options' => $this->uif_value_delimiters(),
          '#description' => t('Select value delimiter for fields receiving multiple values.'),
        ];

        $preview_count = array_combine(
          [0, 1, 10, 100, 1000, 10000, 9999999], [
            0,
            1,
            10,
            100,
            1000,
            10000,
            9999999,
          ]
        );
        $preview_count[0] = t('None - just do it');
        $preview_count[9999999] = t('Preview all');

        $form['preview_count'] = [
          '#type' => 'select',
          '#title' => t('Users to preview'),
          '#default_value' => 10,
          '#options' => $preview_count,
          '#description' => t('Number of users to preview before importing. Note: If you run out of memory set this lower or increase your memory.'),
        ];

        // @FIXME
        // url() expects a route name or an external URI.
        // $form['notify'] = array(
        //         '#type' => 'checkbox',
        //         '#title' => t('Notify new users of account'),
        //         '#description'   => t('If checked, each newly created user will receive the <em>Welcome, new user created by administrator</em> email using the template on the <a href="@url1">user settings page</a>. This is the same email sent for <a href="@url2">admin-created accounts</a>.', array('@url1' => url('admin/user/settings'), '@url2' => url('admin/user/user/create'))),
        //       );
        $form['next'] = [
          '#type' => 'submit',
          '#value' => t('Next'),
        ];

        // Set form parameters so we can accept file uploads.
        $form['#attributes'] = [
          'enctype' => 'multipart/form-data',
        ];
        break;

      case 2:
        $form['instructions'] = [
          '#markup' => t('Preview these records and when ready to import click Import users.'),
          '#prefix' => '<div id="uif_form_instructions">',
          '#suffix' => '</div>',
        ];
        $form['user_preview'] = [
          '#markup' => $form_state->getStorage(),
          '#prefix' => '<div id="uif_user_preview">',
          '#suffix' => '</div>',
        ];
        $form['back'] = [
          '#type' => 'submit',
          '#value' => t('Back'),
          '#submit' => [
            'uif_import_form_back',
          ],
        ];
        $form['submit'] = [
          '#type' => 'submit',
          '#value' => t('Import users'),
        ];
        break;
    }

    return $form;
  }

  /**
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $step = !$form_state->getStorage() ? 1 : $form_state->getStorage();

    switch ($step) {
      case 1:
        // Validate the upload file.
        $validators = [
          'file_validate_extensions' => [
            'csv',
          ],
          'file_validate_size' => [file_upload_max_size()],
        ];

        //if ($user_file = file_save_upload('user_upload', $validators)) {
          //print_r($user_file->uri);
         // die;
          $result = $this->uif_validate_user_file($user_file->uri, $form_state);

          if (!empty($errors)) {
            $form_state->setErrorByName('user_upload', '<ul><li>' . implode('</li><li>', $errors) . '</li></ul>');
            return;
          }
        //}

        // Save the validated data to avoid reparsing.
        $form_state->setStorage($result['data']);
        break;
    }
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $step = !$form_state->getStorage() ? 1 : $form_state->getStorage();

    if (1 == $step) {
      $form_state->setRebuild(TRUE);
      $form_state->setStorage(!$form_state->getValue(['notify']) ? $form_state->getValue(['notify']) : FALSE);
      $form_state->setStorage($form_state->getValue(['field_delimiter']));
      $form_state->setStorage($form_state->getValue(['value_delimiter']));

      $preview_count = $form_state->getValue(['preview_count']);
      if ($preview_count) {
        $form_state->setStorage($preview_count);
        // @FIXME
        // theme() has been renamed to _theme() and should NEVER be called directly.
        // Calling _theme() directly can alter the expected output and potentially
        // introduce security issues (see https://www.drupal.org/node/2195739). You
        // should use renderable arrays instead.
        //
        //
        // @see https://www.drupal.org/node/2195739
        // $form_state['storage']['user_preview'] = theme('uif_preview_users', array('data' => $form_state['storage']['data'], 'limit' => $preview_count));
      }
      else {
        $step = 2;
      }
    }

    if (2 == $step) {
      $form_state->setRebuild(TRUE);
      uif_batch_import_users($form_state);
    }

    $form_state->setStorage($step + 1);
  }


  /**
   * Read the user import file and validate on the way.
   *
   * @param $uri
   *    filepath to the user import file
   * @param $data
   *    returns with array of users
   *
   * @return
   *    FALSE if no errors found
   *    array of error strings if error found
   */
  public function uif_validate_user_file($uri, $form_state) {

    $data = [];
    $data['user'] = [];
    $line = 0;
    $delimiter = $form_state->getValue('field_delimiter');

    // Without this fgetcsv() fails for Mac-created files
    ini_set('auto_detect_line_endings', TRUE);

    if ($fp = fopen($uri, 'r')) {
      // Read the header and allow alterations
      $header_row = fgetcsv($fp, NULL, $delimiter);
      $header_row = uif_normalize_header($header_row);
      uif_adjust_header_values($header_row);
      drupal_alter('uif_header', $header_row);
      $line++;

      $errors = module_invoke_all('uif_validate_header', $header_row, $form_state);
      uif_add_line_number($errors, $line);
      if (!empty($errors)) {
        return $errors;
      }
      $data['header'] = $header_row;

      // Gather core and entity field info
      $data['fields'] = uif_get_field_info($header_row);

      // Read the data
      $data['errors'] = [];
      while (!feof($fp) && (count($errors) < 20)) {
        // Read a row and allow alterations
        $row = fgetcsv($fp, NULL, $delimiter);
        drupal_alter('uif_row', $row, $header_row);
        $line++;

        if (uif_row_has_data($row)) {
          $user_row = uif_clean_and_key_row($header_row, $row, $line);
          $args = [':mail' => db_like($user_row['mail'])];
          $uid = db_query_range('SELECT uid FROM {users} WHERE mail LIKE :mail', 0, 1, $args)->fetchField();

          $more_errors = module_invoke_all('uif_validate_user', $user_row, $uid, $header_row, $form_state);
          uif_add_line_number($more_errors, $line);
          $errors = array_merge($errors, $more_errors);
          $data['user'][] = $user_row;
        }
      }

      // Any errors?
      if (!empty($data)) {
        return $data;
      }
    }
    else {
      return $data['errors'] = t('Cannot open that import file.');
    }

    // Final validation opportunity after header and all users validated individually.
    $errors = module_invoke_all('uif_validate_all_users', $data['user'], $form_state);
    if (!empty($errors)) {
      return $errors;
    }
  }

  /**
   * Field delimiter options.
   */
  public function uif_field_delimiters() {
    return [
      ',' => ',',
      ';' => ';',
      '|' => '|',
    ];
  }

  /**
   * Value delimiter options.
   */
  public function uif_value_delimiters() {
    return [
      '|' => '|',
      ':' => ':',
      '_:_' => '_:_',
      '-*-' => '-*-',
    ];
  }

  /**
   * Implementation of hook_uif_supported_fields().
   *
   * Provide out-of-box supported fields.
   */
  public function uif_supported_fields() {
    //    $subs = array(
    //      '@strtotime_url' => 'http://php.net/manual/en/function.strtotime.php',
    //      '@tz_url' => url('admin/config/regional/settings'),
    //      '@mods_url' => url('admin/modules'),
    //      '@flds_url' => url('admin/config/people/accounts/fields'),
    //    );

    return [
      'mail' => [
        'type' => 'core',
        'required' => TRUE,
        'description' => t('the user\'s email address'),
      ],
      'name' => [
        'type' => 'core',
        'description' => t('a name for the user. If not provided, a name is created based on the email.'),
      ],
      'pass' => [
        'type' => 'core',
        'description' => t('a password for the user. If not provided, a password is generated.'),
      ],
      'roles' => [
        'type' => 'core',
        'description' => t('roles for the user as delimited text, e.g. "admin|editor" (without quotes).'),
      ],
      'created' => [
        'type' => 'core',
        'description' => t('the creation date for the user in <a href="@strtotime_url">strtotime()</a> format.'),
        'parser' => 'uif_get_strtotime_value',
      ],
      'access' => [
        'type' => 'core',
        'description' => t('the last access date for the user in <a href="@strtotime_url">strtotime()</a> format.'),
        'parser' => 'uif_get_strtotime_value',
      ],
      'login' => [
        'type' => 'core',
        'description' => t('the last login date for the user in <a href="@strtotime_url">strtotime()</a> format.'),
        'parser' => 'uif_get_strtotime_value',
      ],
      'status' => [
        'type' => 'core',
        'description' => t('the account status (1 = active (default) 0 = blocked).'),
      ],
      'timezone' => [
        'type' => 'core',
        'description' => t('the time zone to use for this user. You should <a href="@tz_url">let users set their time zone</a> if you import this.'),
      ],
      'language' => [
        'type' => 'core',
        'description' => t('the language to use for this user. You should <a href="@mods_url">enable the locale module</a> if you import this.'),
      ],
      'uid' => [
        'type' => 'core',
        'description' => t('the uid of the user (experts only; use mail as unique key, not uid)'),
      ],
    ];
  }

}
