<?php

namespace Drupal\import_user\Helper;

/**
 *
 */
class ImportUserHelper {

  /**
   *
   */
  public function field_delimiter_options() {
	$options = array(',', ':', '|');
	return array_combine($options, $options);    
  }
  
  /**
   *
   */
  public function value_delimiter() {
	$options = array('|', ':', '_:_', '-*-');
	return array_combine($options, $options);	    
  }

}
